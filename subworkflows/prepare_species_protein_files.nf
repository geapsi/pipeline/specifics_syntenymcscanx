#!/usr/bin/env nextflow

include { gunzip  as gunzipGFF } from '../modules/gunzip.nf'
include { gunzip  as gunzipProtein } from '../modules/gunzip.nf'
include { gunzip  as gunzipChrConv } from '../modules/gunzip.nf'
include { gunzip  as gunzipFAI } from '../modules/gunzip.nf'
include { convertChrNames } from '../modules/convertChrNames.nf'
include { checkGFF } from '../modules/checkGFF.nf'

workflow prepare_species_protein_files {

    take:
    species_protein_files // file: species_protein_files.tsv

    main:
    Channel
        .fromPath(species_protein_files, checkIfExists:true)
        .splitCsv(header:true, sep:"\t")
        .map{ row -> tuple(row.ID, file(row.protein), file(row.gff3), file(row.chr_conversion), file(row.fai)) }
        .multiMap {
            protein:   [it[0], it[1]]
            gff3:     [it[0], it[2]]
            chr_conv: [it[0], it[3]]
            fai: [it[0], it[4]]
        }
        .set { species_data }

    // CHECK IF GZ COMPRESSED, IF SO UNCOMPRESS
    protein_for_gunzip_ch = species_data.protein
        .branch { ID, protein ->
            gunzip: protein.name.endsWith('.gz')
            skip: true
        }
    gunzipProtein(protein_for_gunzip_ch.gunzip)
    gunzipProtein.out.gunzip.mix(protein_for_gunzip_ch.skip)
    .set { uncompressed_protein_ch }

    gff3_for_gunzip_ch = species_data.gff3
        .branch { ID, gff3 ->
            gunzip: gff3.name.endsWith('.gz')
            skip: true
        }
    gunzipGFF(gff3_for_gunzip_ch.gunzip)
    gunzipGFF.out.gunzip.mix(gff3_for_gunzip_ch.skip)
    .set { uncompressed_gff3_ch }

    chr_conv_for_gunzip_ch = species_data.chr_conv
        .branch { ID, chr_conv ->
            gunzip: chr_conv.name.endsWith('.gz')
            skip: true
        }
    gunzipChrConv(chr_conv_for_gunzip_ch.gunzip)
    gunzipChrConv.out.gunzip.mix(chr_conv_for_gunzip_ch.skip)
    .set { uncompressed_chr_conv_ch }

    fai_for_gunzip_ch = species_data.fai
        .branch { ID, fai ->
            gunzip: fai.name.endsWith('.gz')
            skip: true
        }
    gunzipFAI(fai_for_gunzip_ch.gunzip)
    gunzipFAI.out.gunzip.mix(fai_for_gunzip_ch.skip)
    .set { uncompressed_fai_ch }


    if ( params.convert_chr ) {
        genome_dummy_file = file("$projectDir/assets/NO_FILE", checkIfExists: false)
        def genome_dummy_ch = uncompressed_gff3_ch.map { id, genome_file ->
            tuple(id, genome_dummy_file)
        }
        convertChrNames(genome_dummy_ch
            .join(uncompressed_gff3_ch)
            .join(uncompressed_chr_conv_ch))
        uncompressed_gff3_ch = convertChrNames.out.gff3
    }

    if ( params.check_gff ) {
        checkGFF(uncompressed_gff3_ch)
        uncompressed_gff3_ch = checkGFF.out.gff3
    }

    emit:
    protein = uncompressed_protein_ch
    gff3 = uncompressed_gff3_ch
    fai = uncompressed_fai_ch
}