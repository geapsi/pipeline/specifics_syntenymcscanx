#!/usr/bin/env nextflow

include { gunzip  as gunzipGenome } from '../modules/gunzip.nf'
include { gunzip  as gunzipGFF } from '../modules/gunzip.nf'
include { gunzip  as gunzipChrConv } from '../modules/gunzip.nf'
include { convertChrNames } from '../modules/convertChrNames.nf'
include { samtoolsFaidx } from '../modules/samtoolsFaidx.nf'
include { checkGFF } from '../modules/checkGFF.nf'
include { extractCDS } from '../modules/extractCDS.nf'
include { getLongestIsoform } from '../modules/getLongestIsoform.nf'
include { translateCDS } from '../modules/translateCDS.nf'


workflow prepare_species_genome_files {

    take:
    species_genome_files // file: species_genome_files.tsv

    main:

    // Collect path from species file containing ID (species abreviation), genome (fasta) and annotation (gff3)
    Channel
        .fromPath(species_genome_files, checkIfExists:true)
        .splitCsv(header:true, sep:"\t")
        .map{ row -> tuple(row.ID, file(row.genome), file(row.gff3), file(row.chr_conversion)) }
        .multiMap {
            genome:   [it[0], it[1]]
            gff3:     [it[0], it[2]]
            chr_conv: [it[0], it[3]]
        }
        .set { species_data }

    // CHECK IF GZ COMPRESSED, IF SO UNCOMPRESS
    genome_for_gunzip_ch = species_data.genome
        .branch { ID, genome ->
            gunzip: genome.name.endsWith('.gz')
            skip: true
        }
    gunzipGenome(genome_for_gunzip_ch.gunzip)
    gunzipGenome.out.gunzip.mix(genome_for_gunzip_ch.skip)
    .set { uncompressed_genome_ch }

    gff3_for_gunzip_ch = species_data.gff3
        .branch { ID, gff3 ->
            gunzip: gff3.name.endsWith('.gz')
            skip: true
        }
    gunzipGFF(gff3_for_gunzip_ch.gunzip)
    gunzipGFF.out.gunzip.mix(gff3_for_gunzip_ch.skip)
    .set { uncompressed_gff3_ch }


    chr_conv_for_gunzip_ch = species_data.chr_conv
        .branch { ID, chr_conv ->
            gunzip: chr_conv.name.endsWith('.gz')
            skip: true
        }
    gunzipChrConv(chr_conv_for_gunzip_ch.gunzip)
    gunzipChrConv.out.gunzip.mix(chr_conv_for_gunzip_ch.skip)
    .set { uncompressed_chr_conv_ch }


    if ( params.convert_chr ) {
        convertChrNames(uncompressed_genome_ch
            .join(uncompressed_gff3_ch)
            .join(uncompressed_chr_conv_ch))
        uncompressed_genome_ch = convertChrNames.out.genome
        uncompressed_gff3_ch = convertChrNames.out.gff3
    }
    samtoolsFaidx(uncompressed_genome_ch)

    if ( params.check_gff ) {
        checkGFF(uncompressed_gff3_ch)
        uncompressed_gff3_ch = checkGFF.out.gff3
    }

    getLongestIsoform(uncompressed_genome_ch.join(uncompressed_gff3_ch))
    extractCDS(uncompressed_genome_ch.join(getLongestIsoform.out))
    translateCDS(extractCDS.out)

    emit:
    genome = uncompressed_genome_ch
    fai = samtoolsFaidx.out
    gff3 = uncompressed_gff3_ch
    protein = translateCDS.out
}