# Synteny pipeline
This Nextflow pipeline can produce orthogroups from OrthoFinder and synteny blocks from MCScanX, using genome and associated annotation files!

![metro](fig/metro_v2.png)

# Requirements
2 modes are available. The first one uses both the genome (FASTA) and the annotation file (GFF3) to extract proteins and runs the rest of the pipeline (NOTE: make sure that sequences from the genome FASTA are folded, it is a requirement for AGAT/BioPerl). The second mode takes directly proteins as an input, along with the GFF3 file. This second mode gives more control over the proteins of interest and saves some run time.
## First mode (Genome + Annotation)
Genome files should be in FASTA format.
Annotation files should be in GFF3 format.
A TSV file is the main input, gathering paths of aforementioned files for each species. The format is as follow:
```
ID    genome                  gff3               chr_conversion
sp1   input/sp1_genome.fa     input/sp1.gff3     input/chr_conversion/sp1.tsv
sp2   input/sp2_genome.fa     input/sp2.gff3     input/chr_conversion/sp2.tsv
```
The ID of each species should be unique and will be used to name output files. The `chr_conversion` column should indicate the path to a two-columns file for each species and change chromosome names in the FASTA and GFF3. This column is optional and files can be a placeholder, BUT the header must remain.
Please see the `example_data/synteny_genome_infiles.tsv` for a working template.
## Second mode (Protein + Annotation)
Protein files should be in FASTA format.
Annotation files should be in GFF3 format.
A TSV file is the main input, gathering paths of aforementioned files for each species. The format is as follow:
```
ID    protein          gff3             chr_conversion  fai
sp1   input/sp1.fa     input/sp1.gff3   input/chr_conversion/sp1.tsv    input/sp1.fai
sp2   input/sp2.fa     input/sp2.gff3   input/chr_conversion/sp2.tsv    input/sp1.fai
```
The ID of each species should be unique and will be used to name output files. The `chr_conversion` column should indicate the path to a two-columns file for each species and change chromosome names in the GFF3. The fai column should indicate the path to the genome FAI of the associated species (required for synteny coverage evaluation).
Please see the `example_data/synteny_protein_infiles.tsv` for a working template.

# Running the pipeline
An example dataset is provided to test the pipeline.
With the first mode:
```
nextflow run main.nf -c example_data/example_data.config --convert_chr false --species_genome_files example_data/synteny_genome_infiles.tsv --outdir results_example_data/
```
With the second mode:
```
nextflow run main.nf -c example_data/example_data.config --species_protein_files example_data/synteny_protein_infiles.tsv --outdir results_example_data/
```
Default parameters are described in the `nextflow.config` file. User can either modify the `nextflow.config`, create a new config file passed with the `-c` option, or specify parameters with their value in the command line.

# Output
Output files will be gathered in the outdir directory.
If all `publish results` are set to `true` in the config file, the following outputs are expected for each species mentionned in the tsv file in the `Requirements` section:

- checked_gff/ -> GFF3 file after verification with agat_convert_sp_gxf2gxf.pl (if `check_gff` is `true`)
- converted_chr_names/ -> old to new chromosome names (if `convert_chr` is `true`)
- cds/ -> extracted CDS using agat_sp_extract_sequences.pl
- longest_isoform_gff/ -> selection of the longest isoform using agat_sp_keep_longest_isoform.pl (publish is set to `false` by default)
- proteins/ -> translated CDS using seqkit translate
- orthology/ -> OrthoFinder's results
- synteny/ -> MCScanX's results and statistics of the run in statistics/
More output directories can be created to check the results of each process, by changing the related options in the `nextflow.config` file.
