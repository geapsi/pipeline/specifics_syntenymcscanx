#!/usr/bin/env nextflow

nextflow.enable.dsl = 2

include { prepare_species_genome_files } from './subworkflows/prepare_species_genome_files.nf'
include { prepare_species_protein_files } from './subworkflows/prepare_species_protein_files.nf'
include { keepProtInOG } from './modules/keepProtInOG.nf'
include { parseBlastFiles } from './modules/parseBlastFiles.nf'
include { parseGFF } from './modules/parseGFF.nf'
include { runOrthoFinder } from './modules/runOrthoFinder.nf'
include { runMCScanX } from './modules/runMCScanX.nf'
include { getChromosomeSizes } from './modules/getChromosomeSizes.nf'
include { getSyntenyStatistics } from './modules/getSyntenyStatistics.nf'


/*
========================================================================================
    geapsi/pipeline/specifics_syntenymcscanx
========================================================================================
    Gitlab : https://forgemia.inra.fr/geapsi/pipeline/specifics_syntenymcscanx
----------------------------------------------------------------------------------------
*/


def helpMessage() {
    log.info"""
    ==========================================
        Synteny pipeline

        Usage:
        nextflow run synteny_pipeline.nf --species_genome_files input/synteny_infiles.tsv --outdir ./results
    -------------------------------------------

    Required arguments:
    --species_genome_files  tsv file display input files path with format:
                    ID  genome  gff3    chr_conversion
                    sp1 input/sp1_genome.fa     input/sp1.gff3  input/sp1_chr_conv.tsv
                    sp2 input/sp2_genome.fa     input/sp2.gff3  input/sp2_chr_conv.tsv

                    Paths of files are not constrained, as well as their extension. However, the file format is fixed.

    Optional arguments:
    --outdir        directory to output results (default: ./results)
    -work-dir       directory for the temporary files (default: ./work)
    See other optional arguments in the config file and prefix them with a double dash (i.e. --s_match_size 10)
    """
    .stripIndent()
}

// Show help message
if (params.help){
    helpMessage()
    exit(0)
}

workflow {
    // USE GENOME AND GFF TO GET PROTEINS
    if (params.species_genome_files) {
        log.info "paths from file : ${params.species_genome_files}"
        prepare_species_genome_files(params.species_genome_files)
        prot_ch = prepare_species_genome_files.out.protein
        gff3_ch = prepare_species_genome_files.out.gff3
        fai_ch = prepare_species_genome_files.out.fai

    } else if (params.species_protein_files) {
        log.info "paths from file : ${params.species_protein_files}"
        prepare_species_protein_files(params.species_protein_files)
        prot_ch = prepare_species_protein_files.out.protein
        gff3_ch = prepare_species_protein_files.out.gff3
        fai_ch = prepare_species_protein_files.out.fai
    } else {
        error "Invalid mode, choose either species_genome_files or protein_species_genome_files"
    }

    // collect all proteome to a single channel to give to OrthoFinder
    prot_ch.map { it -> it[1] }
        .collect()
        .set { ready_prot_ch }

    //*******************//
    // Orthogroups comp. //
    //*******************//
    runOrthoFinder(ready_prot_ch)
    runOrthoFinder.out.blasts_ch
        .flatten()
        .combine(runOrthoFinder.out.sequence_ids_ch)
        .set{ tuple_blast_seq_id_ch }

    parseBlastFiles(tuple_blast_seq_id_ch)

    // assign a copy of N0 to each blast to parse
    parseBlastFiles.out.blast_with_initial_ids
        .flatten()
        .combine(runOrthoFinder.out.N0_ch)
        .set{ tuple_blast_N0_ch }
    keepProtInOG(tuple_blast_N0_ch)

    keepProtInOG.out.filtered_blast
        .collectFile(name: 'all_parsed_inOG.tsv', newLine: false)
        .set{ merged_blast_ch }

    parseGFF(gff3_ch)
    parseGFF.out.annotated_gff
        .collectFile(name: 'all_parsed_gff.tsv', newLine: false) { it[1] }
        .set{ merged_gff_ch }
    //*******************//
    //  Syntenic blocks  //
    //*******************//
    runMCScanX(merged_blast_ch, merged_gff_ch)
    getChromosomeSizes(fai_ch)
    getChromosomeSizes.out.chr_size
        .collectFile(name: 'all_chr_sizes.tsv', newLine: false) { it[1] }
        .set{ merged_chr_sizes_ch }
    getSyntenyStatistics(runMCScanX.out.collinearity, merged_gff_ch, merged_chr_sizes_ch)
}
