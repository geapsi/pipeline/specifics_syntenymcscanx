import argparse
import os
import re
from collections import defaultdict
from collections import Counter
from datetime import datetime
import pandas as pd

def parse_conversion_table(conv_table):
    """ Get dic with old: new chr patterns from conv table
    @param conv table    the conv table file

    @return  a dic with old: new
    """
    d = {}
    for line in conv_table:
        if line.startswith('#'):
            continue
        row = line.strip().split('\t')
        pattern_old = row[0]
        pattern_new = row[1]
        d[pattern_old] = pattern_new
    return d

def get_old_chr_key(chr, d):
    """ Search for the current chr in d
    @param chr    the current chr string
    @param d      the dic with old : new chr

    @return  the key in d that matches chr
    """
    old_chr = None
    if chr in d.keys():
        old_chr = chr
    else:
        # if using regular expression
        for key in d.keys():
            if key in chr:
                old_chr = key
                break

    return old_chr

def parse_gff(gff, d, output):
    """ Parse the gff and write its version with new chromosomes
    @param gff    gff3 file
    @param d      dic with old: new chr info
    @output       a gff with new chromosome names

    @return  none
    """
    l_old_chr = []
    l_chr_match = []
    for line in gff:
        if line.startswith('#'):
            output.write(line)
            continue
        row = line.split('\t')
        old_chr = get_old_chr_key(row[0], d)
        if old_chr is not None:
            new_chr = re.sub(old_chr, d[old_chr], row[0])
            row[0] = new_chr
            output.write('\t'.join(row))
            if old_chr not in l_old_chr:
                l_old_chr.append(old_chr)
                l_chr_match.append(','.join([old_chr,
                        new_chr]))
        else:
                output.write(line)
    return l_chr_match

def parse_fasta(fasta, d, output_fasta):
    """ Parse the fasta and write its version with new chromosomes
    @param fasta    fasta file
    @param d        dic with old: new chr info
    @param output   the file name to write to

    @return  a list of strings with the carried out chr names transformations
    """
    l_chr_match = []
    for line in fasta:
        if line.startswith('>'):
            row = line.strip().split(' ')
            current_chr = row[0].replace('>', '')
            old_chr = get_old_chr_key(current_chr, d)
            if old_chr is not None:
                new_chr = re.sub(old_chr, d[old_chr], row[0])
                row[0] = new_chr
                output_fasta.write(' '.join(row) + '\n')
                l_chr_match.append(','.join([current_chr,
                    new_chr.replace('>', '')]))
            else:
                output_fasta.write(line)
        else:
            output_fasta.write(line)
            continue
    return l_chr_match

################################################################################


def seqids_to_df(sequenceids):
    """ Reads a SequenceIDs file from OrthoFinder (format: "0_1: Ca_v2.0_24918.1")
    @param sequenceids a file of SequenceIDs

    @return     a pandas df with the two columns
    """
    d = {}
    for line in sequenceids:
        l_line = line.split(':')
        orthofinder_id = l_line[0]
        rna_id = ':'.join([i.strip() for i in l_line[1::]])
        d[orthofinder_id] = rna_id
    df = pd.DataFrame(list(d.items()))

    return df

def filter_blast_top_n_hits(df_blast, top_n_hits):
    """ Filter BLAST files to keep the top n hits each gene, ordered by evalue
    @param df_blast a pd of BLAST file
    @param top_n_hits   top n hits per gene (int/float)

    @return     the df with original identifiers
    """
    df_blast_top_n = (
        df_blast.sort_values(by=['qseqid', 'evalue'])
        .groupby('qseqid')
        .head(top_n_hits)
        .reset_index(drop=True)
    )

    return df_blast_top_n

def filter_blast_evalue(df_blast, eval_threshold):
    """ Filter BLAST files to keep hits with an evalue inf to the threshold
    @param df_blast a pd of BLAST file
    @param eval_threshold   evalue threshold (int/float)

    @return     the df with original identifiers
    """
    df_blast_filtered = df_blast[df_blast['evalue'] <= eval_threshold]

    return df_blast_filtered

def translate_orthofinder_names(df_blast, df_seq_ids):
    """ Translate OrthoFinder blast ids back to the original format
    @param df_blast a pd of BLAST file

    @return     the df with original identifiers
    """
    d_seq_ids = dict(df_seq_ids.values)
    # create Series with converted names using the dic
    col_query_seqid = df_blast['qseqid'].map(d_seq_ids)
    col_subject_seqid = df_blast['sseqid'].map(d_seq_ids)
    df_blast['qseqid'] = col_query_seqid
    df_blast['sseqid'] = col_subject_seqid

    return df_blast

################################################################################

def parse_N0(line):
    """ Parse line from N0.tsv
    @param line line to parse

    @return     the parsed line as a comma-separated string
    """
    if line.startswith('HOG'):
        newline = ','.join(line.split())
        return None
    newline = re.split(',|\t', line.strip())
    # remove the "N0.H" before orthogroup number, for reading simplicity
    newline[0] = newline[0].replace('N0.H','')
    # remove the correspondance to old OG and the n0/- field
    newline.pop(1)
    newline.pop(1)
    newline = ','.join([x.strip().replace(',', '') for x in newline])
    return newline

def get_dic_from_og(og_file):
    """ From N0.tsv creates a dic with orthogroups and their constituants
    @param og_file N0.tsv file

    @return  the dictionnary with OG as key and the set of proteins as value
    """
    d_og = defaultdict(set)
    for line in og_file:
        line_p = parse_N0(line)
        if line_p is not None:
            row = line_p.split(',')
            og = row.pop(0)
            # removing empty strings
            l_prot_in_og = [x for x in row if x]
            d_og[og] = set(l_prot_in_og)

    return d_og

def get_dic_from_blast(blast_file):
    """ From blast file, creates a dictionnary
    @param  blast_file   blast_file a basic blast file

    @return a list of tuples, each containing the two proteins as a set
            and the raw blast line as a string
    """
    l_blast = []
    for line in blast_file:
        if line.startswith('#'):
            continue
        row = line.split('\t')
        prot1 = row[0]
        prot2 = row[1]
        l_blast.append(({prot1, prot2}, line))

    return l_blast

def filter_blast(l_blast, d_og):
    """ Check whether the pair in l_blast is part of the same orthogroup
    @param l_blast  a list of tuples including the protein pair as a set
                    and blast line as a string
    @param d_og     dictionnary with OG as key and the set of proteins as value

    @return         the list of blast lines where proteins belong to the same OG
    """
    l_blast_filtered = []
    for l in l_blast:
        for og in d_og.keys():
            blast_pair = l[0]
            if blast_pair.issubset(d_og[og]):
                blast_line = l[1]
                l_blast_filtered.append(blast_line)
    return l_blast_filtered

############################### SYNTENY ########################################
# from the Ortho_KB pipeline
def get_mcscanx_options(line, d):
    """ From collinearity file extracts parameters of mcscanx run
    @param line line from collinearity file
    @param d  dictionnary storing parameters

    @return  all d with found parameter on the line
    """
    pattern = r'# (.+):'
    parameter = re.search(pattern, line).group(1).lower().replace(' ', '_')
    d[parameter] = line.split(' ')[-1].strip()

    return d

# from the Ortho_KB pipeline
def get_run_statistics(line, d):
    """ From collinearity file line extracts stats of mcscanx run
    @param line line from collinearity file
    @param d  dictionnary storing stats

    @return  all d with found stats on the line
    """
    pattern = ' (.+):'
    row = line.strip().split(',')
    for s in row:
        if s != '':
            stat = re.search(pattern, s).group(1).lower()
            d[stat] = s.split(':')[-1].strip()
    return d

# from the Ortho_KB pipeline
def get_block_info(line, prefix):
    """ From collinearity file line extracts block information of mcscanx run
    @param line line from collinearity file
    @param prefix add the prefix before the block id to ensure unicity

    @return  all d with found information on the block
    """
    d = {}
    pattern_alignment = r'Alignment ([0-9]+):'
    pattern_score = r'score=([0-9-\.]+)'
    pattern_evalue = r'e_value=([0-9-\.-e-]+) '
    pattern_nb = r'N=([0-9]+) '
    pattern_chr = r'N=[0-9]+ (.+) '
    pattern_strand = r' (plus|minus)'

    row = line.strip()
    chromosome = re.search(pattern_chr, row).group(1)

    d['block_id'] = prefix + '_' + re.search(pattern_alignment, row).group(1)
    d['score'] = float(re.search(pattern_score, row).group(1))
    d['evalue'] = float(re.search(pattern_evalue, row).group(1))
    d['nb_genes'] = int(re.search(pattern_nb, row).group(1))
    d['chrA'] = chromosome.split('&')[0]
    d['chrB'] = chromosome.split('&')[1]
    d['flipped'] = re.search(pattern_strand, row).group(1).replace('plus', 'False').replace('minus', 'True')

    return d

def parse_mcscanx_gff_like_to_df(gff_file):
    """ Get a dic with rna_id:gene_id
    @param gff_file    pseudo-gff file (chromosome \t gene \t start \t end)

    @return  d_gff a nested dic with the gene and its chromosome, start and end
    """
    d_gff = {}

    for line in gff_file:
        if (line.startswith('#') or len(line.strip()) == 0):
            continue
        l_line = line.strip().split('\t')
        gene = l_line[1]
        d_gff[gene] = {}
        d_gff[gene]['chromosome'] = l_line[0]
        d_gff[gene]['start'] = int(l_line[2]) + 1 # these are bed coordinates
        d_gff[gene]['end'] = int(l_line[3])
        d_gff[gene]['genome'] = l_line[4]
    return d_gff

def get_block_limits(df_block_gene_coords):
    """Infer syntenic block borders from min of gene start and max of gene end
    @param df_synteny_info df with synteny info (includes block_id)
    @param df_genes df of genes with coordinates info (includes block_id)

    @return  blocks with coordinates on each chr
    """
    df_block_start = df_block_gene_coords.groupby(['block_id', 'chromosome'])['start'].min().reset_index()
    df_block_end = df_block_gene_coords.groupby(['block_id', 'chromosome'])['end'].max().reset_index()
    df_block_limits = df_block_start.merge(df_block_end,
        how='left', on=['block_id', 'chromosome'])
    df_block_limits['block_length'] = (df_block_limits['end'] - df_block_limits['start'] + 1) # bed coords
    return df_block_limits

def get_synteny_pairwise_basic_stats(df_block_info, mode):
    """Infer syntenic block borders from min of gene start and max of gene end
    @param df_block_info df with synteny info (includes block_id, block length, nb_genes...)
    @param mode  pairwise comparison level (either 'genome_pair' or 'chromosome_pair')

    @return  returns a df with stats
    """
    if mode == 'genome_pair':
        mode = ['genome_pair']
    elif mode == 'chromosome_pair':
        mode = ['genome_pair', 'chromosome_pair']
    else:
        print('Wrong pairwise comparison mode', mode)
    df_stats = df_block_info.copy()
    df_stats['block_count'] = (
        df_block_info
        .groupby(mode)['block_id']
        .transform('nunique')
        )
    df_stats['block_length_mean'] = (
        df_block_info
        .groupby(mode + ['genome'])['block_length']
        .transform('mean')
        .round(2)
        )
    df_stats['block_length_min'] = (
        df_block_info
        .groupby(mode + ['genome'])['block_length']
        .transform('min')
        )
    df_stats['block_length_max'] = (
        df_block_info
        .groupby(mode + ['genome'])['block_length']
        .transform('max')
        )
    df_stats['block_length_sd'] = (
        df_block_info
        .groupby(mode + ['genome'])['block_length']
        .transform('std')
        .round(2)
        )
    df_stats['block_gene_count_mean'] = (
        df_block_info
        .groupby(mode + ['genome'])['nb_genes']
        .transform('mean')
        .round(2)
        )
    df_stats['block_gene_count_min'] = (
        df_block_info
        .groupby(mode + ['genome'])['nb_genes']
        .transform('min')
        .round(2)
        )
    df_stats['block_gene_count_max'] = (
        df_block_info
        .groupby(mode + ['genome'])['nb_genes']
        .transform('max')
        .round(2)
        )
    df_stats['block_gene_count_sd'] = (
        df_block_info
        .groupby(mode + ['genome'])['nb_genes']
        .transform('std')
        .round(2)
        )
    if mode == ['genome_pair']:
        coverage_over = 'genome_size'
        group_add = []
    else:
        coverage_over = 'chromosome_size'
        group_add = ['chromosome']

    df_stats['total_block_length'] = (
        df_block_info
        .groupby(mode + ['genome'] + group_add)[['block_length']]
        .transform('sum')
        )
    df_synteny_coverage = (
        df_stats
        .groupby(mode + ['genome'] + group_add)[['total_block_length', coverage_over]]
        .apply(lambda x: (x['total_block_length'] / x[coverage_over]) * 100)
        .round(2)
        .reset_index(level=mode+['genome'])
        .rename(columns={0:'synteny_coverage'})
        )

    df_stats = df_stats.merge(df_synteny_coverage, how='left', on=mode+['genome'])
    df_stats = df_stats[mode + ['genome', 'synteny_coverage', 'block_count', 'block_length_mean', 'block_length_min',
        'block_length_max', 'block_length_sd', 'block_gene_count_mean', 'block_gene_count_min',
        'block_gene_count_max', 'block_gene_count_sd']].drop_duplicates()
    return df_stats
################################################################################
