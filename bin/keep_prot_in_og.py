#!/usr/bin/env python3

"""
Parse a blast file to only keep proteins part of the same orthogroup in N0.tsv.
IDs in the blast and the orthogroups file should match.
Compatible with v.2.5.4 of OrthoFinder.

Usage: TODO
"""

import argparse
import os
from collections import Counter
from datetime import datetime
import logging
from synteny_pipeline_functions import parse_N0
from synteny_pipeline_functions import get_dic_from_og
from synteny_pipeline_functions import get_dic_from_blast
from synteny_pipeline_functions import filter_blast

parser = argparse.ArgumentParser(description = "Filters blast files to ensure \
    both proteins are part of the same orthogroup in N0.tsv")
parser.add_argument('-blast', help = 'blast file to parse', required = True)
parser.add_argument('-orthogroups', help = 'N0.tsv file', required = True)
parser.add_argument('-outfile', help = 'output file name', required = False, \
    default = 'parsed_blast.out')
parser.add_argument('-logs', help = 'if logs are desired', required = False, \
    action = 'store_true')
args = parser.parse_args()

if __name__ == "__main__":

    startTime = datetime.now()
    if args.logs:
        logfile = 'log_' + os.path.basename(args.blast) + \
            datetime.now().strftime('_keep_prot_og_%H_%M_%S_%d_%m_%Y.log')
        logging.basicConfig(filename = logfile, level = logging.INFO)
    cnt = Counter()

    with open(args.blast, 'r') as blast_file:
        l_blast = get_dic_from_blast(blast_file)

    with open(args.orthogroups, 'r') as orthogroups_file:
        d_og = get_dic_from_og(orthogroups_file)
    cnt['orthogroups'] = len(d_og.keys())

    # check if blast (query,subject) in the same OG
    l_blast_filtered = filter_blast(l_blast, d_og)
    cnt['protein_pairs_in_same_orthogroup'] = len(l_blast_filtered)
    cnt['protein_pairs_not_in_same_orthogroup'] = len(l_blast) - \
        len(l_blast_filtered)

    with open(args.outfile, 'w') as ofl:
        [ofl.write(line) for line in l_blast_filtered]

    if args.logs:
        logging.info('File: ' + args.blast)
        logging.info(cnt)
        logging.info('Time spent %s',(datetime.now() - startTime))
