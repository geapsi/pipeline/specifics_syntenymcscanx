#!/usr/bin/env python3

"""
Creates a summary file of syntenic blocks statistics.
Compatible with the output of MCScanX only.


Usage: python3 get_summary_syntenic_blocks.py -c synt.collinearity -g synt.gff
"""
import os
import argparse
import pandas as pd
from synteny_pipeline_functions import get_mcscanx_options, get_run_statistics, \
    get_block_info, get_block_limits, parse_mcscanx_gff_like_to_df, get_synteny_pairwise_basic_stats

parser = argparse.ArgumentParser(description='Create summary statistics for MScanX synteny over genome and chromosomes')
parser.add_argument('-c', '--collinearity',
    help='input collinearity file from MCScanX', required=True)
parser.add_argument('-g', '--gff',
    help='input gff-like file (bed coordinates) given to MCScanX', required=True)
parser.add_argument('-s', '--size',
    help='3-col tsv file of contig name, size (bp) and genome ID', required=True)
parser.add_argument('-p', '--prefix',
    help='prefix for block identifiers', required=False, default='mcscanx')
parser.add_argument('-o', '--output',
    help='output directory for statistic files', required=False, default='statistics')
args = parser.parse_args()

if __name__ == "__main__":
    if not os.path.exists(args.output):
        os.makedirs(args.output)
    else:
        print(f"Output directory already exists: {args.output}")

    with open(args.collinearity, 'r', encoding='utf-8') as synteny_file:
        d_param = {} # stores parameters used for the run
        d_stat = {} # stores very general stats provided by MCScanX
        l_block_info = [] # blocks with their info
        l_block_genes = [] # genes in blocks

        for line in synteny_file:
            if line.startswith('#'):
                if 'Parameters' in str(line):
                    section = 'parameters'
                elif ('Statistics' in str(line)):
                    section = 'statistics'
                elif ('##########################################' in str(line)): # end of Statistics section
                    section = 'alignments'
                elif (section == 'parameters'):
                    d_param = get_mcscanx_options(line, d_param)
                elif (section == 'statistics'):
                    d_stat = get_run_statistics(line, d_stat)
                elif str(line).startswith('## Alignment') and \
                    (section == 'alignments'):
                    d_block_info = get_block_info(line, args.prefix)
                    l_block_info.append(d_block_info)
            elif section == 'alignments' and not line.startswith('#'):
            # adding genes in the list of the block
                l_line = line.strip().split('\t')

                block_id = d_block_info['block_id']
                geneA = l_line[1]
                geneB = l_line[2]
                evalue = float(l_line[3].strip())
                l_block_genes.append([block_id, geneA, geneB, evalue])
            else:
                print("Unknwon MCScanX file format")

    df_synt_blocks = pd.DataFrame(l_block_info)
    del l_block_info
    df_synt_blocks['chromosome_pair'] = df_synt_blocks['chrA'] + '_' + df_synt_blocks['chrB']

    df_synt_blocks_genes = pd.DataFrame(l_block_genes,
        columns=['block_id', 'geneA', 'geneB', 'gene_pair_evalue'])
    del l_block_genes

    # GET CHROMOSOME SIZES
    df_chr_sizes = pd.read_csv(args.size, sep='\t', names=['chromosome', 'chromosome_size', 'genome'])
    df_chr_sizes['genome_size'] = df_chr_sizes.groupby('genome')['chromosome_size'].transform('sum')

    # GET GENE POSITIONS
    with open(args.gff, 'r', encoding='utf-8') as gff_file:
        d_gff = parse_mcscanx_gff_like_to_df(gff_file)
    df_gff = pd.DataFrame.from_dict(d_gff, orient = 'index').rename_axis('gene').reset_index()
    del d_gff

    df_synt_blocks_genes['gene_pair_nb'] = df_synt_blocks_genes.groupby('block_id').cumcount()+1 # cumcounts starts at 0

    df_block_gene_coords = df_synt_blocks_genes.melt(
        id_vars=['block_id', 'gene_pair_nb'],
        value_vars=['geneA', 'geneB'],
        var_name='variable',
        value_name='gene'
    )
    df_block_gene_coords = df_block_gene_coords.drop(columns=['variable']).merge(df_gff, on='gene')
    del df_gff

    df_block_limits = get_block_limits(df_block_gene_coords)
    df_block_limits = df_block_limits.rename(columns={'start':'block_start', 'end':'block_end'})
    df_block_all_info = df_block_gene_coords.merge(df_block_limits, how='left',
        on=['block_id', 'chromosome']).merge(df_synt_blocks[[
            'block_id', 'score', 'evalue', 'nb_genes', 'flipped', 'chromosome_pair']])

    # output a more complete file than the synt.collinearity file
    genome_pairs = (
        df_block_all_info[['block_id', 'genome', 'chromosome_pair']]
        .drop_duplicates()
        .groupby(['block_id', 'chromosome_pair'])['genome']
        .apply(lambda x: '_'.join(sorted(x)) if len(x.unique()) > 1 else f"{x.iloc[0]}_{x.iloc[0]}")
        .reset_index(drop=False)
        .rename(columns={'genome':'genome_pair'})
        .drop(columns=['chromosome_pair'])
    )
    df_block_all_info = df_block_all_info.merge(
        genome_pairs, how='left', on='block_id'
        )
    df_block_all_info.to_csv(os.path.join(args.output, 'collinearity_enriched.csv'), index=False)

    # get general stats
    d_stat['overall number of blocks'] = df_block_all_info['block_id'].nunique()
    d_stat['overall mean gene count per block'] = float(
        df_block_all_info[['block_id', 'nb_genes']].drop_duplicates()['nb_genes'].mean().round(2)
        )
    d_stat['overall min gene count per block'] = int(
        df_block_all_info[['block_id', 'nb_genes']].drop_duplicates()['nb_genes'].min()
        )
    d_stat['overall max gene count per block'] = int(
        df_block_all_info[['block_id', 'nb_genes']].drop_duplicates()['nb_genes'].max()
        )
    d_stat['overall sd gene count per block'] = float(
        df_block_all_info[['block_id', 'nb_genes']].drop_duplicates()['nb_genes'].std().round(2)
        )
    d_stat['overall mean block size (bp)'] = float(
        df_block_all_info[['block_id', 'block_length']].drop_duplicates()['block_length'].mean().round(2)
        )
    d_stat['overall min block size (bp)'] = int(
        df_block_all_info[['block_id', 'block_length']].drop_duplicates()['block_length'].min().round(2)
        )
    d_stat['overall max block size (bp)'] = int(
        df_block_all_info[['block_id', 'block_length']].drop_duplicates()['block_length'].max().round(2)
        )
    d_stat['overall sd block size (bp)'] = float(
        df_block_all_info[['block_id', 'block_length']].drop_duplicates()['block_length'].std().round(2)
        )
    d_stat['intra-genome number of blocks'] = int(
        df_block_all_info[['block_id', 'genome']]
        .drop_duplicates()
        .groupby('block_id')['genome']
        .nunique().eq(1).sum()
        )
    d_stat['inter-genome number of blocks'] = int(
        df_block_all_info[['block_id', 'genome']]
        .drop_duplicates()
        .groupby('block_id')['genome']
        .nunique().eq(2).sum()
        )


    df_block_all_info = df_block_all_info.merge(df_chr_sizes, how='left', on=['genome', 'chromosome'])

    df_block_info_no_genes = (df_block_all_info[['block_id', 'genome_pair', 'genome', 'genome_size', 'chromosome_pair', 'chromosome',
        'chromosome_size', 'block_start', 'block_end', 'block_length', 'nb_genes',
        'score', 'evalue', 'flipped']]
        .drop_duplicates()
        .reset_index(drop=True)
        .sort_values(by=['block_id']))

    # per genome statistics
    df_stats_per_genome_pair = get_synteny_pairwise_basic_stats(
        df_block_info_no_genes, 'genome_pair'
        )
    df_stats_per_genome_pair.to_csv(os.path.join(args.output, 'synteny_statistics_per_genome_pair.csv'),
        index=False, header=True)
    # per chromosome statistics
    df_stats_per_chromosome_pair = get_synteny_pairwise_basic_stats(
        df_block_info_no_genes, 'chromosome_pair'
        )
    df_stats_per_chromosome_pair.to_csv(os.path.join(args.output, 'synteny_statistics_per_chromosome_pair.csv'),
        index=False, header=True)

    df_stats = pd.DataFrame.from_dict(d_stat, orient='index')
    df_stats.to_csv(os.path.join(args.output, 'general_statistics.csv'), header=False)
