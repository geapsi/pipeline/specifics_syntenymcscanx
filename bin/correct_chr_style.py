#!/usr/bin/env python3

"""
Parse chromosomes and scaffolds from gff and fasta using a provided conversion
table.
Useful to have distinct chromosome names for SynVisio or just simpler names.

Usage: python3 bin/correct_chr_style.py
        -gff data/lcul_subset_selected_genes_annotation.gff3
        -fa data/lcul_subset_genome.fa
        -chr data/lcul_renaming.tsv
"""

import argparse
import re
from synteny_pipeline_functions import parse_conversion_table
from synteny_pipeline_functions import get_old_chr_key
from synteny_pipeline_functions import parse_gff
from synteny_pipeline_functions import parse_fasta


parser = argparse.ArgumentParser(description='Parse the genome fasta and gff \
    annotation files to update chromosome names according to the chromosome \
    conversion table')
parser.add_argument('-gff', help='gff3 file of the species', required=True)
parser.add_argument('-fa', help='fasta file of the species genome', required=False)
parser.add_argument('-chr_conv_table', help='conversion table with new and \
    the old format. Can use patterns', required=True)
parser.add_argument('-output_gff', help = 'gff3 file with parsed chromosomes',
    required=False, default='new_chr.gff3')
parser.add_argument('-output_fa', help='fasta file with parsed chromosomes',
    required=False, default='new_chr.fa')
parser.add_argument('-output_chr_match', help='match between old and new \
    format for all sequences, in csv', required=False,
    default='chr_conversion_table.csv')
args = parser.parse_args()


if __name__ == "__main__":
    with open(args.chr_conv_table, 'r', encoding='utf-8') as chr_file:
        d_conv_table = parse_conversion_table(chr_file)

    # write the parsed gff file
    with open(args.output_gff, 'w', encoding='utf-8') as output_gff:
        with open(args.gff, 'r', encoding='utf-8') as gff_file:
            l_chr_match = parse_gff(gff_file, d_conv_table, output_gff)

    # write the parsed fasta file
    if args.fa:
        with open(args.output_fa, 'w', encoding='utf-8') as output_fasta:
            with open(args.fa, 'r', encoding='utf-8') as fasta_file:
                l_chr_match = parse_fasta(fasta_file, d_conv_table, output_fasta)

    # write all lines from the conversion table that were actually found in
    # the fasta file or the gff file if no fasta provided
    with open(args.output_chr_match, 'w', encoding='utf-8') as chr_match:
        for pair in l_chr_match:
            chr_match.write(pair + '\n')
