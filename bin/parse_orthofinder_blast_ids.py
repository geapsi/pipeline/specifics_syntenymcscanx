#!/usr/bin/env python3

"""
OrthoFinder blast files have ids such as 0_0 or 1_2345 where the first part is
the species and the second is a number for the sequence.
Here going back to the original sequence id, found in
the WorkingDirectory/SequenceIDs.txt file of the OrthoFinder folder.
Compatible with v.2.5.4 of OrthoFinder.
Also allows to filter Blast files to keep hits with a specific evalue or a top n
of the best hits in the file.

Usage: TODO
"""

import argparse

from synteny_pipeline_functions import translate_orthofinder_names, seqids_to_df, \
    filter_blast_top_n_hits, filter_blast_evalue

import pandas as pd
pd.set_option('display.max_columns', None)


parser = argparse.ArgumentParser(description='Parse blast file')
parser.add_argument('-blast', help='blast file from OrthoFinder', required=True)
parser.add_argument('-seqids', help='file from Orthofinder with \
    new : original seq id (called SequenceIDs.txt)', required=True)
parser.add_argument('-spids', help='file with \
    new : original sp name (called Species.txt)', required=False)
parser.add_argument('-evalue', help='filter blast hits with an evalue \
    superior or equal to this threshold', required=False, default=float('inf'),
    type=float)
parser.add_argument('-top_n_hits', help='filter blast hits to keep for each gene \
    the first n hits with maximal evalue', required=False, default=float('inf'),
    type=int)
parser.add_argument('-outfile', help='output file name', required=False,
    default='parsed_blast.out')
args = parser.parse_args()


if __name__== "__main__":

    df_blast = pd.read_csv(args.blast, sep='\t', comment='#',
        header=None, skipinitialspace=True, names = ['qseqid', 'sseqid',
                                                        'pident', 'length',
                                                        'mismatch', 'gapopen',
                                                        'qstart', 'qend',
                                                        'sstart', 'send',
                                                        'evalue', 'bitscore'])
    with open(args.seqids, 'r', encoding='utf-8') as seqids:
        df_seq_ids = seqids_to_df(seqids)

    # keep the top n hits for each gene and/or filter for evalues in general
    df_blast_filtered = filter_blast_top_n_hits(df_blast, args.top_n_hits)
    df_blast_filtered = filter_blast_evalue(df_blast_filtered, args.evalue)

    # parsing blast qseqid and sseqid
    df_blast_translated = translate_orthofinder_names(df_blast_filtered, df_seq_ids)
    df_blast_translated.to_csv(args.outfile, index=False, header=None, sep='\t')
