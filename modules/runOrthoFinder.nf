//*************************************************//
//     Launch OrthoFinder to get orthogroups       //
//*************************************************//

process runOrthoFinder {
    //label 'medium'
    conda 'bioconda::orthofinder=2.5.5=hdfd78af_2 bioconda::diamond=2.1.10'
    container 'quay.io/biocontainers/orthofinder:2.5.5--hdfd78af_2'

    input:
    path "*"

    output:
    path "orthofinder"
    path "orthofinder/Results*/Phylogenetic_Hierarchical_Orthogroups/N0.tsv", emit: N0_ch
    path "orthofinder/Results*/WorkingDirectory/Blast*", emit: blasts_ch
    path "orthofinder/Results*/WorkingDirectory/SequenceIDs.txt", emit: sequence_ids_ch

    script:
    def sensitive = params.o_ultra_sensitive == true ? "-S diamond_ultra_sens" : ''
    """
    orthofinder \
    -t ${task.cpus} \
    -I ${params.o_inflate} \
    -f . \
    ${sensitive} \
    -o orthofinder
    """
}
