//*************************************************//
//     Extract chromosome sizes from the FAI for each species  //
//*************************************************//

process getChromosomeSizes {
    //label 'low'

    input:
    tuple val(ID), path(fai)
    output:
    tuple val(ID), path("${ID}_chromosome_size.tsv"), emit: chr_size

    script:
    """
    awk -F"\t" -v species=$ID '{print \$1"\t"\$2"\t"species}' $fai > ${ID}_chromosome_size.tsv
    """
}
