//*************************************************//
//    Launch MCScanX to obtain .collinearity file  //
//*************************************************//

process runMCScanX {
    //label 'medium'
    conda 'bioconda::mcscanx=1.0.0'
    container 'quay.io/biocontainers/mcscanx:1.0.0--h9948957_0'

    input:
    path blast
    path gff
    output:
    path "${params.synteny_prefix}*"
    path "${params.synteny_prefix}.collinearity", emit: collinearity
    path "${params.synteny_prefix}.tandem", emit: tandem, optional: true

    script:
    """
    prefix="${params.synteny_prefix}"
    mv $blast \${prefix}.blast
    mv $gff \${prefix}.gff

    MCScanX \
    ./\$prefix \
    -k ${params.s_match_score} \
    -g ${params.s_gap_penalty} \
    -s ${params.s_match_size} \
    -e ${params.s_evalue} \
    -m ${params.s_max_gaps} \
    -w ${params.s_overlap_window}
    """
}
