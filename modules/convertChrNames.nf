//*************************************************//
//           Convert chr and scaf names            //
//*************************************************//

process convertChrNames {
    //label 'low'
    conda 'conda-forge::pandas=2.2.1'
    container 'quay.io/biocontainers/pandas:2.2.1'

    input:
    tuple val(ID), path(genome), path(gff3), path(chr_convert_table)
    output:
    tuple val(ID), path("${ID}_genome_conv.fa"), optional: true, emit: genome
    tuple val(ID), path("${ID}_annotation_conv.gff3"), emit: gff3
    path "${ID}_match.csv"

    script:
    def genome_file = genome.fileName.name != 'NO_FILE' ? "-fa ${genome}" : ''
    """
    correct_chr_style.py \\
    -gff $gff3 \\
    $genome_file \\
    -chr_conv_table $chr_convert_table \\
    -output_gff ${ID}_annotation_conv.gff3 \\
    -output_fa  ${ID}_genome_conv.fa \\
    -output_chr_match ${ID}_match.csv
    """
}
