//*************************************************//
//    Orthofinder works best if only one isoform   //
//*************************************************//

process getLongestIsoform {
    //label 'low'
    conda 'bioconda::agat=0.9.1'
    container 'quay.io/biocontainers/agat:0.9.1--pl5321hdfd78af_0'

    input:
    tuple val(ID), path(genome), path(gff3)
    output:
    tuple val(ID), path("${ID}.isoform.gff3")

    script:
    """
    agat_sp_keep_longest_isoform.pl \
    --gff $gff3 \
    --output ${ID}.isoform.gff3
    """
}
