//*************************************************//
//          Translate CDS sequences                //
//*************************************************//

process translateCDS {
    //label 'low'
    conda 'bioconda::seqkit=2.3.0'
    container 'quay.io/biocontainers/seqkit:2.3.0--h9ee0642_0'

    input:
    tuple val(ID), path(cds)
    output:
    tuple val(ID), path("${ID}.fa")
    script:
    """
    seqkit translate $cds \
    --frame 1 \
    -M \
    --trim \
    -o ${ID}.fa
    """
}
