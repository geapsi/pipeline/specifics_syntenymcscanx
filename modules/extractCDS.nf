//*************************************************//
//          Extract CDS sequence                   //
//*************************************************//

process extractCDS {
    //label 'medium'
    conda 'bioconda::agat=0.9.1'
    container 'quay.io/biocontainers/agat:0.9.1--pl5321hdfd78af_0'

    input:
    tuple val(ID), path(genome), path(gff3)
    output:
    tuple val(ID), path("${ID}.cds")

    script:
    """
    agat_sp_extract_sequences.pl \
    -g $gff3 \
    -f $genome \
    -t cds \
    -o cds.tmp
    sed -e 's/[[:space:]].*//' cds.tmp > ${ID}.cds
    """
}
