#!/usr/bin/env nextflow

process samtoolsFaidx {
    conda 'bioconda::samtools=1.17'
    container 'quay.io/biocontainers/samtools:1.17--hd87286a_1'

    input:
    tuple val(ID), path(fasta)

    output:
    tuple val(ID), path("*.fai"), emit: fai

    script:
    """
    samtools \\
        faidx \\
        $fasta
    """
}