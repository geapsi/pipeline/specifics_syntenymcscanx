//******************************************************//
// Filter blast files to obtain orthologs-based synteny //
//******************************************************//

process keepProtInOG {
    //label 'high'
    conda 'conda-forge::pandas=1.1.5'
    container 'quay.io/biocontainers/pandas:2.2.1'

    input:
    tuple path(blast_file), path(N0)
    output:
    path '*_inOG.blast', emit: filtered_blast

    script:
    if ( params.s_only_og )
    """
    keep_prot_in_og.py \
    -blast $blast_file \
    -orthogroups $N0 \
    -outfile ${blast_file.baseName}_inOG.blast
    """
    else
    """
    cat $blast_file > ${blast_file.baseName}_inOG.blast
    """
}
