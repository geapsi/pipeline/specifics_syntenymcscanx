//*************************************************//
//     Check gff3 format using agat parser         //
//*************************************************//

process checkGFF {
    conda 'bioconda::agat=0.9.1'
    container 'quay.io/biocontainers/agat:0.9.1--pl5321hdfd78af_0'

    input:
    tuple val(ID), path(gff3)
    output:
    tuple val(ID), path("${ID}.checked.gff3"), emit: gff3
    path '*.agat.log'

    script:
    """
    agat_convert_sp_gxf2gxf.pl \\
    -g $gff3 \\
    -o ${ID}.checked.gff3
    """
}
