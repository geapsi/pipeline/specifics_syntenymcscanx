//*************************************************//
//     Matching MCScanX gff specific requirements  //
//*************************************************//

process parseGFF {
    //label 'low'
    conda 'bioconda::bedops=2.4.39'
    container 'quay.io/biocontainers/bedops:2.4.39--hc9558a2_0'

    input:
    tuple val(ID), path(gff)
    output:
    tuple val(ID), path("${ID}_annotated_gff.tsv"), emit: annotated_gff

    script:
    """
    species=$ID
    awk '\$3 == "mRNA" || \$3 == "RNA" || \$3 == "transcript" {print \$0}' $gff > tmp.gff
    gff2bed < tmp.gff > tmp.bed
    awk -F"\t" -v sp=\$species '\$1!~ "^#" {print \$1"\t"\$4"\t"\$2"\t"\$3"\t"sp}' tmp.bed > ${ID}_annotated_gff.tsv
    """
}
