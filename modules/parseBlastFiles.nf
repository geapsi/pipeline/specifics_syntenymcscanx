//************************************************************//
//  Filter and changing sequences IDs back to the ones in gff //
//************************************************************//

process parseBlastFiles {
    //label 'low'
    conda 'conda-forge::pandas=2.2.1'
    container 'quay.io/biocontainers/pandas:2.2.1'

    input:
    tuple path(zipped_blast), path(seq_id)
    output:
    path '*parsed.txt', emit: blast_with_initial_ids

    script:
    """
    zcat $zipped_blast > blast
    parse_orthofinder_blast_ids.py \
    -blast blast \
    -seqids $seq_id \
    -evalue ${params.s_evalue} \
    -top_n_hits ${params.s_top_n} \
    -outfile ${zipped_blast.baseName}_parsed.txt
    """
}
