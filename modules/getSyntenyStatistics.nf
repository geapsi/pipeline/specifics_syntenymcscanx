//*************************************************//
//    Extract descriptive statistics for synteny   //
//*************************************************//

process getSyntenyStatistics {
    conda 'conda-forge::pandas=2.2.1'
    container 'quay.io/biocontainers/pandas:2.2.1'

    input:
    path collinearity
    path gff // gff-like for input in MCScanX with the last col being the species
    path chr_size // 3-col tsv file of contig name, size (bp) and ID
    output:
    path "statistics" // outputs a series of CSV files for stats

    script:
    """
    get_summary_syntenic_blocks.py \\
    --collinearity $collinearity \\
    --gff $gff \\
    --size $chr_size \\
    --output statistics
    """
}